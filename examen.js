//funcion 

function validar(){
    
    var cedula,nombre,direccion,correo;
    cedula= document.getElementById("cedula").value;
    nombre= document.getElementById("nombre").value;
    direccion= document.getElementById("direccion").value;
    telefono= document.getElementById("telefono").value;
    correo= document.getElementById("correo").value;

    expresioncedula=/^\d{10}$/; //10digitos numericos
    expresiontelefono=/^\d{10}$/; //10digitos numericos
    expresionnombre=/^[a-zA-Z]/; //caracteres para nombre
    expresiondireccion=/^[0-9a-zA-Z]+$/; //Letras y numeros
    expresioncorreo=/^[^@]+@[^@]+\.[a-zA-Z]{2,}$/; //expresion para ingreso de correo

// validaciones de registro usanod expresiones regulares 

    if(!expresioncedula.test(cedula)){
        alert("Nombre no valido");
        return false;
        }
    if(!expresionnombre.test(nombre)){
        alert("Nombre no valido");
        return false;
        }
    if(!expresiondireccion.test(direccion)){
        alert("Nombre no valido");
        return false;
        }
    if(!expresiontelefono.test(telefono)){
        alert("Nombre no valido");
        return false;
        }
    if(!expresioncorreo.test(correo)){
        alert("Nombre no valido");
        return false;
        }

//metodo par registrar los datos
app.post('/examen', async (req, res)=>{
        const cedula = req.body.cedula;
        const nombre = req.body.nombre;
        const direccion = req.body.direccion;
        const telefono = req.body.telefono;
        const correo = req.body.correo;

        connection.query('INSERT INTO registro SET ?',{
            cedula:cedula, 
            nombre:nombre, 
            direccion:direccion, 
            telefono:telefono, 
            correo:correo,
        },
        async (error, results)=>{
                if(error){
                    console.log(error);
                }else{     
                    res.render('registro', {
                        alert: true,
                        alertTitle: "Confirmacion de Registro",
                        alertMessage: "Te has registrado correctamente",
                        alertIcon:'success',
                        showConfirmButton: false,
                        timer: 1500,
                        ruta: ''
                    });     
                }
            });
        })
    }
